(function () {
    angular.module("MyApp").service("MyAppService", MyAppService);

    MyAppService.$inject = ["$http", "$q"];

    function MyAppService($http, $q) {
        var service = this;

        //expose the following services
        service.getImages = getImages;

        function getImages(tag) {

            var defer = $q.defer();

            $http.get("https://api.giphy.com/v1/gifs/search", {
                params: {
                    api_key: "ac419443623246189d35e51eab60c1d9",
                    q: tag,
                    limit: 5,
                    offset: 0
                }
            }).then(function (result) {
                console.log(result.data.data[0]);
                var data = result.data.data;
                var images = []
                for (var i in data) {
                    images.push(data[i].images.fixed_height_small.url);
                    // images.push(data[i].images.downsized_medium.url);      
                }
                var tag = self.tag;
                defer.resolve(images);
            }, function (err) {
                deferred.reject(err);
            });

            return defer.promise;
        }

    }

})();