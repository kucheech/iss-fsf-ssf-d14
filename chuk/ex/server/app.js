const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const NODE_PORT = process.env.PORT || 3000;

const CLIENT_FOLDER = path.join(__dirname, "/../client/");
app.use(express.static(CLIENT_FOLDER));

var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'user',
    password: 'password',
    database: 'sakila'
});

const SELECT_FILM_BY_FILM_ID = "select * from film where film_id = ?";

const handleError = function (err, res) {
    res.status(500).type("text/plain").send(JSON.stringify(err));
}

const mkQuery = function (sql, pool) {
    const sqlQuery = function () {
        var sqlParams = [];
        for (i in arguments) {
            sqlParams.push(arguments[i]);
        }

        const promise = new Promise(function (resolve, reject) {
            pool.getConnection(function (err, conn) {
                if (err) {
                    reject(err);
                    return;
                }

                conn.query(sql, sqlParams, function (err, result) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(result);
                    }

                    conn.release();
                });
            });
        });

        return promise;
    }

    return sqlQuery;
};

const findFilmByFilmId = mkQuery(SELECT_FILM_BY_FILM_ID, pool);


app.get("/film/:filmId", function (req, res) {
    const filmId = parseInt(req.params.filmId); //to convert type 
    if (isNaN(filmId) || filmId < 0) {
        res.status(400).type("text/plain").send("filmId should be a (positive) number");
        return;
    }

    findFilmByFilmId(filmId)
        .then(function (result) {
            if (result.length > 0) {
                const film = result[0];
                res.status(200);
                res.format({
                    "application/json": function () {
                        res.send(film);
                    },
                    "text/plain": function () {
                        res.send("Title: " + film.title);
                    },
                    "default": function () {
                        // log the request and respond with 406
                        res.status(406).send('Not Acceptable');
                    }
                });

            } else {
                res.status(404).type("text/plain").send("Film not found");
            }
        }).catch(function (err) {
            handleError(err, res);
        });
    // pool.getConnection(function (err, conn) {
    //     if (err) {
    //         handleError(err, res);
    //         return;
    //     }

    //     conn.query(SELECT_FILM_BY_FILM_ID, [filmId],
    //         function (err, result) {
    //             if (err) {
    //                 handleError(err, res);
    //             } else {
    //                 if (result.length > 0) {
    //                     const film = result[0];
    //                     res.status(200);
    //                     res.format({
    //                         "application/json": function () {
    //                             res.send(film);
    //                         },
    //                         "text/plain": function () {
    //                             res.send("Title: " + film.title);
    //                         },
    //                         "default": function () {
    //                             // log the request and respond with 406
    //                             res.status(406).send('Not Acceptable');
    //                         }
    //                     });

    //                 } else {
    //                     res.status(404).type("text/plain").send("Film not found");
    //                 }

    //             }

    //             conn.release();

    //         });

});


app.get("/hello", function (req, res) {
    res.status(200).send("hello back");
});

app.get("/films", function (req, res) {

    var deferred = q.defer();

    pool.getConnection(function (err, conn) {
        if (err) {
            conn.release();
            deferred.reject(res.status(500).send(err));
        }

        const SQL_QUERY = "SELECT * FROM film";
        conn.query(SQL_QUERY, function (err, rows, fields) {
            if (err) {
                conn.release();
                console.log(err);
                deferred.reject(res.status(500).send(err));
                throw err;
            }

            console.log(rows);
            // console.log(fields);

            deferred.resolve(res.status(200).send(rows));
            conn.release();
        });
    });

    return deferred.promise;
});

app.get("/users", function (req, res) {
    // console.log("/users");
    var deferred = q.defer();

    pool.getConnection(function (err, conn) {
        if (err) {
            conn.release();
            deferred.reject(res.status(500).send(err));
        }

        const SQL_QUERY = "SELECT * FROM users";
        conn.query(SQL_QUERY, function (err, rows, fields) {
            if (err) {
                conn.release();
                console.log(err);
                deferred.reject(res.status(500).send(err));
                throw err;
            }

            console.log(rows);
            // console.log(fields);

            deferred.resolve(res.status(200).send(rows));
            conn.release();
        });
    });

    return deferred.promise;
});

app.get("/users/:userId", function (req, res) {
    console.log(req.params.userId);
    // console.log("/users");
    var deferred = q.defer();

    // pool.getConnection(function (err, conn) {
    //     if (err) {
    //         conn.release();
    //         deferred.reject(res.status(500).send(err));
    //     }

    //     const SQL_QUERY = "SELECT * FROM users";
    //     conn.query(SQL_QUERY, function (err, rows, fields) {
    //         if (err) {
    //             conn.release();
    //             console.log(err);
    //             deferred.reject(res.status(500).send(err));
    //             throw err;
    //         }

    //         console.log(rows);
    //         // console.log(fields);

    //         deferred.resolve(res.status(200).send(rows));
    //         conn.release();
    //     });
    // });

    // return deferred.promise;
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app