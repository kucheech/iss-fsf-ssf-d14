function add(a, b) {
    return (a + b);
}

var op1 = 43;

var mul = function (a, b) {
    return (a * b);
}

console.log(add);
console.log(mul);

var apply = function (f, a, b) {
    var result = f(a, b);
    console.log(a + " " + f.name + " " + b + " = " + result);
    return result;
}

apply(add, 3, 4);
apply(mul, 3, 4);
apply(mul, apply(add, 1, 2), 4);

var person = { //instantiation
    name: "fred",
    email: "f@stones.com"
}

var flintstones = [];

var mkPerson = function (n, e) {
    if (!e) {
        e = n + "@stones.com";
    }
    return ({ name: n, email: e });
}

var wilma = mkPerson("Wilma", "w@stones.com");
var pebbles = mkPerson("Pebbles");

console.log(wilma);
console.log(pebbles);

var Person = function (n, e) {
    this.name = n;
    this.e = !e ? n + "@stones.com" : e;
}

wilma = new Person("Wilma", "w@stones.com");
pebbles = new Person("Pebbles");


Person.prototype.hello = function () {
    console.log("Hello, my name is " + this.name);
}

//function constructor
var fred = new Person("Fred");

console.log(wilma);
console.log(pebbles);
console.log(fred);

pebbles.hello();

fred.hello = function () {
    console.log("I am " + this.name);
}

fred.hello();
wilma.hello();

var greet = function (name) {
    return "Hello " + name;
}

var g = function (f, a) {
    return f(a);
}

console.log(greet("Fred"));
console.log(greet("Wilma"));

var mkGreeting = function (name) {
    return (function () {
        return "Hello " + name;
    });
}

var greetFred = mkGreeting("Fred");
console.log(greetFred());

var mkPower = function (e) {
    var power = function (b) {
        var result = 1;
        for (var i = 0; i < e; i++) {
            result *= b;
        }
        return result;
    }
    return power;
}

var square = mkPower(2);
var cube = mkPower(3);

console.log(square(9));
console.log(cube(9));

var add = (function () {
    var i = 0;
    return function () { return i += 1; }
})();

console.log(add());
console.log(add());
console.log(add());

var plus = (function () {
    var i = 0;
    return function () { return i += n; }
})();

console.log(plus(5));
console.log(plus(3));